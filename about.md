---
title: Sobre
layout: teams
description: Somos um coletivo de tecnologia digital autônomo que se propõe em desenvolver ferramentas de software para auxiliar a solucionar problemas socioambientais, agroecologia, economia solidária e de desigualdade social...
permalink: "/about/"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

Somos um coletivo de tecnologia digital autônomo que se propõe em desenvolver ferramentas de software para auxiliar a solucionar problemas socioambientais, agroecologia, economia solidária e de desigualdade social.

Assim como um pequeno pedaço de madeira que se acomoda na carne, uma farpa, somos aquele pequeno e inquieto incomodo  alojado na camada social em busca de auxiliar processos de transformação.

