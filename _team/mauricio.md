---
title: "Mauricio Manix"
date: 2018-11-19T10:47:58+10:00
draft: false
jobtitle: "Engenheiro de Software"
linkedinurl: ""
promoted: true
weight: 3
layout: team
---
Mauricio Manix, Cientista Social, Arte-Educador e violeiro, graduando em Análise e Desenvolvimento de Sistemas e vivendo a jornada de aprender programação, entusiasta do Debian e do Software Livre,  Radioamador de apartamento e interessado por gambiarras em geral.