---
title: "Andre Formiga"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/formiga.jpg"
jobtitle: "Engenheiro de Software"
linkedinurl: ""
promoted: true
weight: 1
layout: team
---

É engenheiro de software e comunicador popular. Graduado em análise de Sistemas pela FATEC-RS e esta cursando MBA em Engenharia de Dados na instituição IGTI. Um dos idealizadores do coletivo de tecnologia FARPA. Ativista no movimento de software livre e conhecimento livre. Colaborador ativo do hackerspace MateHackers e no coletivo AOS (Autonomia OpenSource). Desenvolve habilidades com alimentação viva, agricultura urbana, música e arte de rua.
