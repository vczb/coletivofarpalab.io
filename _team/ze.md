---
title: "Zé"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/vagner.jpg"
jobtitle: "UX, Designer"
linkedinurl: ""
promoted: true
weight: 20
layout: team
---
Meu nome é zé, também chamado José Victor.

Graduado em estatística pela USP de São Carlos, gosto de usar dados para entender melhor nosso mundo e tomar decisões mais embasadas.

Trabalhou últimos 3 anos e meio com pesquisa e desenvolvimento em uma agritech, atuando na análise e tratamento de dados, programação de hardware e experimentação em laboratório.

Desde 2016 sou voluntário na organização da CryptoRave e também faço parte do Coletivo Encripta. Nessas áreas, fez palestras, treinamentos e consultorias relacionadas a privacidade, software livre e segurança holística.

Nos últimos meses também tem se aproximado da Teia dos Povos de São Paulo.

Tem interesse em formas de construir autonomia individual e coletiva, usando as tecnologia.