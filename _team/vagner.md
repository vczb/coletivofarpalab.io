---
title: "Vagner Ribas"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/vagner.jpg"
jobtitle: "UX, Designer"
linkedinurl: ""
promoted: true
weight: 4
layout: team
---

Vagner Ribas, UX, Designer, Front End, Cofundador do Matehakers, Cofundador do GUFERS, ajudou a criar o kaaete.org, publicitário pela PUCRS, já trabalhou na criação e no desenvolvimento de startups. Entusiasta em novos tecnologias, imagina um mundo mais sustentável e livre. Adora o mar e as coisa da natureza.
